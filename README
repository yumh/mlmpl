                            MLMPL 2.0
                            =========

mlmpl is a small perl script to manage mailing list.

This is the second version of mlmpl.

This repository contains mda.pl that is the main script that do all
the works and mlmctl.pl that is a cli frontend to manage mailing
lists, subscriptions and admins.


                             Features
                             --------

 * multi mailing list on multiple domains, no restrictions
 * archive in a maildir-like fashion
 * read-only, public and private mailing lists
 * automatic management of subcriptions/unsubscriptions (and reply)
 * fully configurable
 * small codebase, easy to hack


                             Install
                             -------

Just drop mda.pl somewhere and use your SMTP daemon to deliver the
emails to it.  As an example, using OpenSMTPD:

	accept from any for domain "foo.net" alias <foo> deliver \
		to mda "/etc/mlmpl/mda.pl %{rcpt} ml@foo.net %{sender}" \
		as foobar


                          Configuration
                          -------------

Both mda.pl and mlmctl.pl will read the config from $MLMPL_CONFIG
or /etc/mlmpl/config.pl . You'll find a sample of the config in
this repo, but it's small enough to be included here:

	our $archive_dir = "/tmp/archive";
	our $db_file = "./db.sqlite";
	our $mail_template_dir = "mail-templates";

	our $smtpserver = "localhost";
	our $smtpport = 25;


                            Quickstart
                            ----------

 1. Choose a dir where to install mlmpl, I'll assume it's /etc/mlmpl,
    but can be anywhere

 2. Generate a sqlite db somewhere, I'll assume it's in /etc/mlmpl/db.sqlite

	cd /etc/mlmpl
	sqlite db.sqlite < /path/to/schema.sql

 3. Copy the config file and customize the content. Please note that
    $archive_dir MUST NOT ends with a '/'.

 4. Create the mailing list(s) with mlmctl.pl
    Note: execute ``./mlmpl.pl help'' to see how to use it.

 5. Tell your SMTP daemon to deliver the mail to mlmpl' mda.pl


                            TODO list
                            ---------

 * support for authentication when sending email (easy to add)
 * more commands?
 * ...

                               FAQ
                               ---

 -> Why have you written this:

I needed a simple solution to manage a couple of small-ish mailing
lists and I found the other tools too much complex. After all, mlmpl
do what it's supposed to do and it's pretty small:

	$ wc -l *.pl
	  6 config.pl
	256 mda.pl
	275 mlmctl.pl
	537 total

