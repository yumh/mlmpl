our $archive_dir = "/tmp/archive";
our $db_file = "./db.sqlite";
our $mail_template_dir = "mail-templates";

our $smtpserver = "localhost";
our $smtpport = 25;
